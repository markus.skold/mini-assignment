from functools import wraps

def add_logging(prefix):
    def _add_logging(fn):
        @wraps(fn)
        def logger():
            with open("test.txt",'w',encoding = 'utf-8') as f:
                f.write('running function x') # till loggfil
                f.write(f'{prefix} running function x') # till loggfil
            return fn()
        return logger
    return _add_logging



@add_logging('Done')
def my_fn():
    '''THis is a docstring'''
    print('a')
    return 'a'


my_fn()